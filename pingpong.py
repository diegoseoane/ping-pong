import turtle
import os

#Pantalla
wn = turtle.Screen()
wn.title("Ping Pong")
wn.bgcolor("black")
wn.setup(width=800, height=600)
wn.tracer(1)

#player1
player1 = turtle.Turtle()
player1.speed(0)
player1.shape("square")
player1.color("white")
player1.penup()#penup es para que no dibuje una linea
player1.goto(-350,0)
#por defecto el tamaño es 20x20, acá multiplicamos x 5
player1.shapesize(stretch_wid=5, stretch_len=1)

#player2
player2 = turtle.Turtle()
player2.speed(0)
player2.shape("square")
player2.color("white")
player2.penup()
#se adecuan las posiciones de acuerdo al tamaño de la pantalla
player2.goto(350,0)
#por defecto el tamaño es 20x20, acá multiplicamos x 5
player2.shapesize(stretch_wid=5, stretch_len=1)


#Pelota
pelota = turtle.Turtle()
pelota.speed(0)
pelota.shape("square")
pelota.color("white")
pelota.penup()
#se adecuan las posiciones de acuerdo al tamaño de la pantalla
pelota.goto(0,0)
#la pelota se mueve de a 2 pixeles
pelota.dx = 4
pelota.dy = 4

#Pelota2
pelota2 = turtle.Turtle()
pelota2.speed(0)
pelota2.shape("square")
pelota2.color("blue")
pelota2.penup()
#se adecuan las posiciones de acuerdo al tamaño de la pantalla
pelota2.goto(0,0)
#la pelota se mueve de a 2 pixeles
pelota2.dx = -3
pelota2.dy = -3

#lapiz
lapiz = turtle.Turtle()
lapiz.speed(0)
lapiz.color("white")
lapiz.penup()
lapiz.hideturtle()
lapiz.goto(0,260)
lapiz.write("Player 1: 0    Player 2: 0", align="center", font=("Courier", 24, "normal"))

#puntos
puntos1 = 0
puntos2 = 0

#funciones
def player1_subir():
    y = player1.ycor()
    y += 20
    player1.sety(y)

def player1_bajar():
    y = player1.ycor()
    y -= 20
    player1.sety(y)

def player2_subir():
    y = player2.ycor()
    y += 20
    player2.sety(y)

def player2_bajar():
    y = player2.ycor()
    y -= 20
    player2.sety(y)

#teclado
wn.listen()
wn.onkeypress(player1_subir, "w")
wn.onkeypress(player1_bajar, "s")
wn.onkeypress(player2_subir, "Up")
wn.onkeypress(player2_bajar, "Down")




# loop principal
while True:
    wn.update()

    #movimiento de la pelota
    pelota.setx(pelota.xcor() + pelota.dx)
    pelota.sety(pelota.ycor() + pelota.dy)

    pelota2.setx(pelota2.xcor() + pelota2.dx)
    pelota2.sety(pelota2.ycor() + pelota2.dy)

    #bordes
    if pelota.ycor() < -290:
        pelota.sety(-290)
        pelota.dy *= -1
        os.system("aplay pong.wav&")

    if pelota.ycor() > 290:
        pelota.sety(290)
        pelota.dy *= -1
        os.system("aplay pong.wav&")
        # mac: os.system("afplay pong.wav&")
        # windows: import winsound
        #          winsound.PlaySound("pong.wav", winsound.SND_ASYNC)

    if pelota.xcor() > 390:
        pelota.goto(0,0)
        pelota.dx *= -1
        #puntos
        puntos1 += 1
        lapiz.clear()
        lapiz.write("Player 1: {}    Player 2: {}".format(puntos1, puntos2), align="center", font=("Courier", 24, "normal"))

    if pelota.xcor() < -390:
        pelota.goto(0,0)
        pelota.dx *= -1
        #puntos
        puntos2 += 1
        lapiz.clear()
        lapiz.write("Player 1: {}    Player 2: {}".format(puntos1, puntos2), align="center", font=("Courier", 24, "normal"))

    if pelota2.ycor() < -290:
        pelota2.sety(-290)
        pelota2.dy *= -1
        os.system("aplay pong.wav&")

    if pelota2.ycor() > 290:
        pelota2.sety(290)
        pelota2.dy *= -1
        os.system("aplay pong.wav&")
        # mac: os.system("afplay pong.wav&")
        # windows: import winsound
        #          winsound.PlaySound("pong.wav", winsound.SND_ASYNC)

    if pelota2.xcor() > 390:
        pelota2.goto(0,0)
        pelota2.dx *= -1
        #puntos
        puntos1 += 1
        lapiz.clear()
        lapiz.write("Player 1: {}    Player 2: {}".format(puntos1, puntos2), align="center", font=("Courier", 24, "normal"))

    if pelota2.xcor() < -390:
        pelota2.goto(0,0)
        pelota2.dx *= -1
        #puntos
        puntos2 += 1
        lapiz.clear()
        lapiz.write("Player 1: {}    Player 2: {}".format(puntos1, puntos2), align="center", font=("Courier", 24, "normal"))

    #colision pelota y jugadores
    if (pelota.xcor() > 340 and pelota.xcor() < 350) and (pelota.ycor() < player2.ycor() + 40 and pelota.ycor() > player2.ycor() - 40):
        pelota.setx(340)
        pelota.dx *= -1
    
    if (pelota.xcor() < -340 and pelota.xcor() > -350) and (pelota.ycor() < player1.ycor() + 40 and pelota.ycor() > player1.ycor() - 40):
        pelota.setx(-340)
        pelota.dx *= -1
    
    if (pelota2.xcor() > 340 and pelota2.xcor() < 350) and (pelota2.ycor() < player2.ycor() + 40 and pelota2.ycor() > player2.ycor() - 40):
        pelota2.setx(340)
        pelota2.dx *= -1
    
    if (pelota2.xcor() < -340 and pelota2.xcor() > -350) and (pelota2.ycor() < player1.ycor() + 40 and pelota2.ycor() > player1.ycor() - 40):
        pelota2.setx(-340)
        pelota2.dx *= -1

    #AI
    if pelota.xcor() > pelota2.xcor():
        if player2.ycor() > pelota.ycor() and abs(player2.ycor() - pelota.ycor() > 10):
            player2_bajar()
        elif player2.ycor() < pelota.ycor() and abs(pelota.ycor() - player2.ycor() > 10):
            player2_subir()
    else:
        if player2.ycor() > pelota2.ycor() and abs(player2.ycor() - pelota2.ycor() > 10):
            player2_bajar()
        elif player2.ycor() < pelota2.ycor() and abs(pelota2.ycor() - player2.ycor() > 10):
            player2_subir()
